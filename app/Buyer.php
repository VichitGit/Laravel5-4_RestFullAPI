<?php

namespace App;

use App\Transaction;

class Buyer extends User
{
    public function transactions()
    {
    	//One to Many Relationship
        return $this->hasMany(Transaction::class);
    }
}
