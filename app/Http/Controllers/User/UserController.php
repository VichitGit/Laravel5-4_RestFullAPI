<?php

namespace App\Http\Controllers\User;

use App\User;
use function bcrypt;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use function response;

class UserController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * find all user
     * localhost:8000/user
     */
    public function index()
    {
        //get all user
        $users = User::all();

        // return response()->json(['data' => $users], 200);

        return $this->showAll($users);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //insert 
    public function store(Request $request)
    {
        //validation 
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:3|confirmed',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerificationCode();
        $data['admin'] = User::REGULAR_USER;

        $user = User::created($data);

        // return response()->json(['data' => $user], 201);

        return $this->showOne($user, 201);
    }


    /**
     * find user by ID
     * localhost:8000/user/1
     */

    public function show($id)
    {
        //get by id user
        $user = User::findOrFail($id);
        // return response()->json(['data' => $user], 200);

        return $this->showOne($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $rule = [
            'email' => 'email|unique:user,email,' . $user->id,
            'password' => 'min:4|confirmed',
            'admin' => 'in:' . User::ADMIN_USER . ',' . User::REGULAR_USER,
        ];

        if ($request->has('name')) {

            $user->name = $request->name;

        }


        if ($request->has('email') && $user->email != $request->email) {
            $user->verified = User::UNVERIFIED_USER;
            $user->veritication_token = User::generateVerificationCode();
            $user->email = $request->email;
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request, password);

        }
        if ($request->has('admin')) {
            if (!$user->isVerified()) {
                return response()->json(['error' => 'only verified users can mify the admin field',
                    'code' => 409], 409);

            }

            $user->admin = $request->admin;
        }


        if (!$user->isDirty()) {

            return response()->json(['error' => 'You need to specify a different values to update code',
                'code' => 422], 422);
        }

        $user->save();

        // return response()->json(['data' => $user], 200);

        return $this->showOne($user);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    //Delete
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        // return response()->json(['data' => $user], 200);

        return $this->showOne($user, 201);


    }

}
