<?php

namespace App;
use App\Product;

class Seller extends User
{
    public function products()
    {
    	//One to Many
        return $this->hasMany(Product::class);
    }
}
